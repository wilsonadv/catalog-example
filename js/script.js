// Get json and build list
$.ajax({
	url : '/catalog.php',
	dataType : 'json',
	success : function(data) {
		$('#categories li').remove();
		$('#categories').append('<li>' + data.categories[0].name + '</li>');
		$('#categories').append('<li>' + data.categories[1].name + '</li>');
		$('#categories').append('<li>' + data.categories[2].name + '</li>');
	}
});