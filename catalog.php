<?php
	// Create a Catalog
	$catalog = array(
		'categories' => array(
			array(
				'name' 			=> 'Category 1',
				'categories'	=> array(
					array(
						'name'			=> 'Category 1 Sub 1',
						'products'		=> array()
					),
					array(
						'name'			=> 'Category 1 Sub 2',
						'categories'	=> array(
							array(
								'name'		=> 'Category 1 Sub 2 Sub 1',
								'products'	=> array()
							),
							array(
								'name'		=> 'Category 1 Sub 2 Sub 2',
								'products'	=> array()
							),
							array(
								'name'		=> 'Category 1 Sub 2 Sub 3',
								'products'	=> array()
							),
						)
					),
					array(
						'name'			=> 'Category 1 Sub 3',
						'products'		=> array()
					),
				)
			),
			array(
				'name' 			=> 'Category 2',
				'products'		=> array()
			),
			array(
				'name' 			=> 'Category 3',
				'categories'	=> array(
					array(
						'name'		=> 'Category 3 Sub 1',
						'products'	=> array()
					),
					array(
						'name'		=> 'Category 3 Sub 2',
						'products'	=> array()
					),
					array(
						'name'		=> 'Category 3 Sub 3',
						'products'	=> array()
					),
				)
			),
		),
	);

	echo json_encode($catalog);
?>