## Quick jQuery Example

Modify script.js to load a nested category list, and fix any other bugs in the meantime. Script.js should be refactored and extended to support an infinite level of nested categories. The final solution should:

* Replace the contents of #categories with a nested unordered list of categories as supplied from catalog.php
* Gracefully handle errors
* Work in browsers as far back as IE7

Catalog.php is simply returning an multi-level array as JSON data, and modifications to it are not necessary for this exercise.

Catalog.php will return a JSON string in the following format:

```json
{
    "categories": [
        {
            "name": "Category 1",
            "categories": [
                {
                    "name": "Category 1 Sub 1",
                    "products": []
                },
                {
                    "name": "Category 1 Sub 2",
                    "categories": [
                        {
                            "name": "Category 1 Sub 2 Sub 1",
                            "products": []
                        },
                        {
                            "name": "Category 1 Sub 2 Sub 2",
                            "products": []
                        },
                        {
                            "name": "Category 1 Sub 2 Sub 3",
                            "products": []
                        }
                    ]
                },
                {
                    "name": "Category 1 Sub 3",
                    "products": []
                }
            ]
        },
        {
            "name": "Category 2",
            "products": []
        },
        {
            "name": "Category 3",
            "categories": [
                {
                    "name": "Category 3 Sub 1",
                    "products": []
                },
                {
                    "name": "Category 3 Sub 2",
                    "products": []
                },
                {
                    "name": "Category 3 Sub 3",
                    "products": []
                }
            ]
        }
    ]
}
```